//
//  JCPrefix.swift
//  JCKit
//
//  Created by C J on 2019/5/8.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

public func JC_CLAMP(_ x: CGFloat, _ low: CGFloat, _ high: CGFloat) -> CGFloat{
   return ((x > high) ? high : ((x < low) ? low : x))
}

public func JCAssertMainThread(){
    assert(Thread.isMainThread)
}

public func JCNSRangeFromCFRange(_ range: CFRange) -> NSRange{
    return NSMakeRange(range.location, range.length)
}

public func JCCFRangeFromNSRange(_ range: NSRange) -> CFRange{
    return CFRangeMake(range.location, range.length)
}

public func JCBenchmark(_ block: ()->Void, _ complete: (Double)->Void) {
    var t0 = timeval()
    var t1 = timeval()
    gettimeofday(&t0, nil)
    block()
    gettimeofday(&t1, nil)
    let tv0 = Double(t1.tv_sec - t0.tv_sec) * 1e3
    let tv1 = Double(t1.tv_usec - t0.tv_usec) * 1e-3
    let ms = tv0 + tv1
    complete(ms)
}

public func dispatch_async_on_main_queue(_ block: @escaping ()->Void) {
    if pthread_main_np() != 0{
        block()
    }else {
        DispatchQueue.main.async {
            block()
        }
    }
}

public func dispatch_sync_on_main_queue(_ block: @escaping ()->Void) {
    if pthread_main_np() != 0{
        block()
    }else {
        DispatchQueue.main.sync {
            block()
        }
    }
}

public let kVersion = UIDevice.current.systemVersion
public let kScreenW = UIScreen.main.bounds.size.width
public let kScreenH = UIScreen.main.bounds.size.height
public let kScale = kScreenW/375.0
public let kStatusBarFrame = UIApplication.shared.statusBarFrame

public let kSafeAreaTopHeight = kStatusBarFrame.height + 44
public let kSafeAreaStatusHeight = kStatusBarFrame.height
public let kSafeAreaBottomHeight = CGFloat(kStatusBarFrame.height != 20 ? 34 : 0)
public let kSafeAreaTabbarHeight = CGFloat(kStatusBarFrame.height != 20 ? 78 : 44)

public func RGBA(r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) -> UIColor{
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}
//16进制颜色
public func kRGBColorFromHex(rgbValue: Int, alpha: CGFloat = 1.0) -> (UIColor) {
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                   alpha: alpha)
}

public func DDDouble(_ num: String) -> Double {
    return Double(num) ?? 0
}

public typealias JCAction = () -> Void
public typealias JCBoolAction = (Bool) -> Void
public typealias JCIntAction = (Int) -> Void
