//
//  JCCGUtilities.swift
//  JCKit
//
//  Created by C J on 2019/5/21.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation
import Accelerate

public func JCCGContextCreateARGBBitmapContext( _ size: CGSize, _ opaque: Bool, _ scale: CGFloat) -> CGContext? {
    return JCCGContextCreateBitmapContext(true, opaque, size, scale)
}

public func JCCGContextCreateGrayBitmapContext(_ size: CGSize, _ scale: CGFloat) -> CGContext?{
    return JCCGContextCreateBitmapContext(false, false, size, scale)
}

private func JCCGContextCreateBitmapContext(_ isARGB: Bool, _ opaque: Bool, _ size: CGSize,  _ scale: CGFloat) -> CGContext? {
    let width = ceil(size.width * scale)
    let height = ceil(size.height * scale)
    if width < 1 || height < 1 {
        return nil
    }
    
    let space = isARGB ? CGColorSpaceCreateDeviceRGB() : CGColorSpaceCreateDeviceGray()
    let alphaInfo = isARGB ? (opaque ? CGImageAlphaInfo.noneSkipFirst : CGImageAlphaInfo.premultipliedFirst) : CGImageAlphaInfo.none
    let context = CGContext.init(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: space, bitmapInfo: CGImageByteOrderInfo.orderDefault.rawValue | alphaInfo.rawValue)
    if context != nil {
        context?.translateBy(x: 0, y: height)
        context?.scaleBy(x: scale, y: -scale)
    }
    return context
}

public func JCScreenScale() -> CGFloat {
    var scale = CGFloat(0)
    DispatchQueue.once(token: "JCScreenScale") {
        scale = UIScreen.main.scale
    }
    return scale
}

public func JCScreenSize() -> CGSize {
    var size = CGSize.zero
    DispatchQueue.once(token: "JCScreenSize") {
        size = UIScreen.main.bounds.size
        if size.height < size.width {
            (size.width, size.height) = (size.height, size.width)
        }
    }
    return size
}

public func matrix_invert(_ matrix: inout [Double]) -> Int {
    let pivot = UnsafeMutablePointer<__CLPK_integer>.allocate(capacity: matrix.count)
    let workspace = UnsafeMutablePointer<Double>.allocate(capacity: matrix.count)
    
    var error : __CLPK_integer = 0
    
    var n = __CLPK_integer(sqrt(Double(matrix.count)))
    var m = n
    var lda = n
    
    dgetrf_(&m, &n, &matrix, &lda, pivot, &error)
    
    if error != 0 {
        return Int(error)
    }
    
    dgetri_(&m, &matrix, &lda, pivot, workspace, &n, &error)
    return Int(error)
}

public func JCCGAffineTransformGetFromPoints(_ before: [CGPoint], after: [CGPoint]) -> CGAffineTransform{
    if before.count < 3 || after.count < 3 {
        return CGAffineTransform.identity
    }
    var p1, p2, p3, q1, q2, q3: CGPoint
    p1 = before[0]; p2 = before[1]; p3 = before[2]
    q1 = after[0]; q2 = after[1]; q3 = after[2]
    var A: [Double] = Array.init(repeating: 0, count: 36)
    A[ 0] = Double(p1.x); A[ 1] = Double(p1.y); A[ 2] = 0; A[ 3] = 0; A[ 4] = 1; A[ 5] = 0
    A[ 6] = 0; A[ 7] = 0; A[ 8] = Double(p1.x); A[ 9] = Double(p1.y); A[10] = 0; A[11] = 1
    A[12] = Double(p2.x); A[13] = Double(p2.y); A[14] = 0; A[15] = 0; A[16] = 1; A[17] = 0
    A[18] = 0; A[19] = 0; A[20] = Double(p2.x); A[21] = Double(p2.y); A[22] = 0; A[23] = 1
    A[24] = Double(p3.x); A[25] = Double(p3.y); A[26] = 0; A[27] = 0; A[28] = 1; A[29] = 0
    A[30] = 0; A[31] = 0; A[32] = Double(p3.x); A[33] = Double(p3.y); A[34] = 0; A[35] = 1
    let error = matrix_invert(&A)
    if error > 0{
        return CGAffineTransform.identity
    }
    var B: [Double] = Array.init(repeating: 0, count: 6)
    B[0] = Double(q1.x); B[1] = Double(q1.y); B[2] = Double(q2.x); B[3] = Double(q2.y); B[4] = Double(q3.x); B[5] = Double(q3.y)
    
    var M: [Double] = Array.init(repeating: 0, count: 6)
    M[0] = A[ 0] * B[0] + A[ 1] * B[1] + A[ 2] * B[2] + A[ 3] * B[3] + A[ 4] * B[4] + A[ 5] * B[5]
    M[1] = A[ 6] * B[0] + A[ 7] * B[1] + A[ 8] * B[2] + A[ 9] * B[3] + A[10] * B[4] + A[11] * B[5]
    M[2] = A[12] * B[0] + A[13] * B[1] + A[14] * B[2] + A[15] * B[3] + A[16] * B[4] + A[17] * B[5]
    M[3] = A[18] * B[0] + A[19] * B[1] + A[20] * B[2] + A[21] * B[3] + A[22] * B[4] + A[23] * B[5]
    M[4] = A[24] * B[0] + A[25] * B[1] + A[26] * B[2] + A[27] * B[3] + A[28] * B[4] + A[29] * B[5]
    M[5] = A[30] * B[0] + A[31] * B[1] + A[32] * B[2] + A[33] * B[3] + A[34] * B[4] + A[35] * B[5]
    
    return CGAffineTransform.init(a: CGFloat(M[0]), b: CGFloat(M[2]), c: CGFloat(M[1]), d: CGFloat(M[3]), tx: CGFloat(M[4]), ty: CGFloat(M[5]))
}

public func JCCGAffineTransformGetFromViews(_ from: UIView, to: UIView) -> CGAffineTransform{
    var before, after: [CGPoint]
    before = Array.init(repeating: CGPoint.zero, count: 3)
    after = Array.init(repeating: CGPoint.zero, count: 3)
    
    before[0] = CGPoint.init(x: 0, y: 0)
    before[1] = CGPoint.init(x: 0, y: 1)
    before[2] = CGPoint.init(x: 1, y: 0)
    after[0] = from.convert(before[0], to: to)
    after[1] = from.convert(before[1], to: to)
    after[2] = from.convert(before[2], to: to)
    
    return JCCGAffineTransformGetFromPoints(before, after: after)
}

public func JCCAGravityToUIViewContentMode(_ gravity: String) -> UIView.ContentMode {
    var dic = NSMutableDictionary.init()
    DispatchQueue.once(token: "JCCAGravityToUIViewContentMode") {
        dic = [CALayerContentsGravity.center.rawValue: UIView.ContentMode.center,
               CALayerContentsGravity.top.rawValue: UIView.ContentMode.top,
               CALayerContentsGravity.bottom.rawValue: UIView.ContentMode.bottom,
               CALayerContentsGravity.left.rawValue: UIView.ContentMode.left,
               CALayerContentsGravity.right.rawValue: UIView.ContentMode.right,
               CALayerContentsGravity.topLeft.rawValue: UIView.ContentMode.topLeft,
               CALayerContentsGravity.topRight.rawValue: UIView.ContentMode.topRight,
               CALayerContentsGravity.bottomLeft.rawValue: UIView.ContentMode.bottomLeft,
               CALayerContentsGravity.bottomRight.rawValue: UIView.ContentMode.bottomRight,
               CALayerContentsGravity.resize.rawValue: UIView.ContentMode.scaleToFill,
               CALayerContentsGravity.resizeAspect.rawValue: UIView.ContentMode.scaleAspectFit,
               CALayerContentsGravity.resizeAspectFill.rawValue: UIView.ContentMode.scaleAspectFill
        ]
    }
    return UIView.ContentMode(rawValue: dic.value(forKey: gravity) as! Int) ?? .scaleToFill
}

public func JCUIViewContentModeToCAGravity(_ contentMode: UIView.ContentMode) -> String {
    switch contentMode {
    case .scaleToFill:
        return CALayerContentsGravity.resize.rawValue
    case .scaleAspectFit:
        return CALayerContentsGravity.resizeAspect.rawValue
    case .scaleAspectFill:
        return CALayerContentsGravity.resizeAspectFill.rawValue
    case .redraw:
        return CALayerContentsGravity.resize.rawValue
    case .bottomRight:
        return CALayerContentsGravity.bottomRight.rawValue
    case .bottomLeft:
        return CALayerContentsGravity.bottomLeft.rawValue
    case .topRight:
        return CALayerContentsGravity.topRight.rawValue
    case .topLeft:
        return CALayerContentsGravity.topLeft.rawValue
    case .right:
        return CALayerContentsGravity.right.rawValue
    case .left:
        return CALayerContentsGravity.left.rawValue
    case .bottom:
        return CALayerContentsGravity.bottom.rawValue
    case .top:
        return CALayerContentsGravity.top.rawValue
    case .center:
        return CALayerContentsGravity.center.rawValue
    default:
        return CALayerContentsGravity.resize.rawValue
    }
}

public func JCCGRectFitWithContentMode(_ rect: inout CGRect, _ size: inout CGSize, _ mode: UIView.ContentMode) -> CGRect {
    rect = rect.standardized
    size.width = size.width < 0 ? -size.width : size.width
    size.height = size.height < 0 ? -size.height : size.height
    let center = CGPoint.init(x: rect.midX, y: rect.midY)
    switch mode {
    case .scaleAspectFit, .scaleAspectFill:
        if rect.size.width < 0.01 || rect.size.height < 0.01 || size.width < 0.01 || size.height < 0.01 {
            rect.origin = center
            rect.size = .zero
        }else {
            var scale = CGFloat(0)
            if mode == .scaleAspectFit {
                if size.width / size.height < rect.size.width / rect.size.height {
                    scale = rect.size.height / size.height
                }else {
                    scale = rect.size.width / size.width
                }
            }else {
                if size.width / size.height < rect.size.width / rect.size.height {
                    scale = rect.size.width / size.width
                }else {
                    scale = rect.size.height / size.height
                }
            }
            size.width *= scale
            size.height *= scale
            rect.size = size
            rect.origin = CGPoint.init(x: center.x - size.width * 0.5, y: center.y - size.height * 0.5)
        }
        break
    case .center :
        rect.size = size
        rect.origin = CGPoint.init(x: center.x - size.width * 0.5, y: center.y - size.height * 0.5)
        break
    case .top :
        rect.origin.x = center.x - size.width * 0.5
        rect.size = size
        break
    case .bottom :
        rect.origin.x = center.x - size.width * 0.5
        rect.origin.y += rect.size.height - size.height
        rect.size = size
        break
    case .left :
        rect.origin.y = center.y - size.height * 0.5
        rect.size = size
        break
    case .right :
        rect.origin.y = center.y - size.height * 0.5
        rect.origin.x += rect.size.width - size.width
        rect.size = size
        break
    case .topLeft :
        rect.size = size
        break
    case .topRight:
        rect.origin.x += rect.size.width - size.width
        rect.size = size
        break
    case .bottomLeft :
        rect.origin.y += rect.size.height - size.height
        rect.size = size
        break
    case .bottomRight :
        rect.origin.x += rect.size.width - size.width
        rect.origin.y += rect.size.height - size.height
        rect.size = size
        break
    default:
        break
    }
    return rect
}
