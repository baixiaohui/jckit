//
//  Timer+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/15.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension Timer {
    public class func jc_scheduledTimer(_ timeInterval: TimeInterval, _ isRepeat: Bool, _ block: @escaping (Timer) -> Void) -> Timer{
        if #available(iOS 10.0, *) {
            return Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: isRepeat) { (timer) in
                block(timer)
            }
        }
        return Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(app_timerAction), userInfo: block, repeats: isRepeat)
    }
    
    @objc private class func app_timerAction(_ sender: Timer) {
        if sender.userInfo != nil {
            let onlyTimerBlock = sender.userInfo as! ((Timer) -> Void)
            onlyTimerBlock(sender)
        }
    }
    
    public class func jc_init(_ timeInterval: TimeInterval, _ isRepeat: Bool, _ block: @escaping (Timer) -> Void) -> Timer{
        if #available(iOS 10.0, *) {
            return Timer.init(timeInterval: timeInterval, repeats: isRepeat) { (timer) in
                block(timer)
            }
        }
        return Timer.init(timeInterval: timeInterval, target: self, selector: #selector(app_timerAction), userInfo: block, repeats: isRepeat)
    }
}
