//
//  Date+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/14.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension Date {
    public var year: Int {
        return getOnComponent(.year)
    }
    
    public var month: Int {
        return getOnComponent(.month)
    }
    
    public var day: Int {
        return getOnComponent(.day)
    }
    
    public var quarter: Int {
        return getOnComponent(.quarter)
    }
    
    public var isLeapMonth: Bool {
        return getComponents(.month, self).isLeapMonth ?? false
        
    }
    
    public var isLeapYear: Bool {
        return ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)))
    }
    
    public var isToday: Bool {
        if (fabs(self.timeIntervalSinceNow) >= 60 * 60 * 24) {
            return false
        }
        return Date().day == self.day
    }
    
    public var isYesterday: Bool {
        let added = self.dateByAddingDays(1)
        return added.isToday
    }
    
    public func getOnComponent(_ component: Calendar.Component, _ date: Date? = nil) -> Int{
        var from = date
        if date == nil {
            from = self
        }
        return Calendar.current.component(component, from: from!)
    }
    
    public func getComponents(_ component: Calendar.Component, _ date: Date) -> DateComponents{
        return Calendar.current.dateComponents(Set<Calendar.Component>.init(arrayLiteral: component), from: date)
    }
    
    public func dateByAddingYears(_ years: Int) -> Date? {
        var components = DateComponents.init()
        components.year = years
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    public func dateByAddingMonths(_ months: Int) -> Date? {
        var components = DateComponents.init()
        components.month = months
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    public func dateByAddingWeeks(_ weeks: Int) -> Date? {
        var components = DateComponents.init()
        components.weekOfYear = weeks
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    public func dateByAddingDays(_ days: Int) -> Date {
        let aTimeInterval = self.timeIntervalSinceReferenceDate + Double(86400 * days)
        let newDate = Date.init(timeIntervalSinceReferenceDate: aTimeInterval)
        return newDate
    }
    
    public func dateByAddingHours(_ hours: Int) -> Date {
        let aTimeInterval = self.timeIntervalSinceReferenceDate + Double(3600 * hours)
        let newDate = Date.init(timeIntervalSinceReferenceDate: aTimeInterval)
        return newDate
    }
    
    public func dateByAddingMinutes(_ minutes: Int) -> Date {
        let aTimeInterval = self.timeIntervalSinceReferenceDate + Double(60 * minutes)
        let newDate = Date.init(timeIntervalSinceReferenceDate: aTimeInterval)
        return newDate
    }
    
    public func dateByAddingSeconds(_ seconds: Int) -> Date {
        let aTimeInterval = self.timeIntervalSinceReferenceDate + Double(seconds)
        let newDate = Date.init(timeIntervalSinceReferenceDate: aTimeInterval)
        return newDate
    }
    
    public func stringWithFormat(_ format: String, _ timeZone: TimeZone = .current, _ locale: Locale = .current) -> String {
        return Date.getFormat(format, timeZone, locale).string(from: self)
    }
    
    public func stringWithISOFormat() -> String{
        var formatter: DateFormatter!
        DispatchQueue.once(token: "jcKit.stringWithISOFormat") {
            formatter = DateFormatter.init()
            formatter.locale = Locale.init(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        return formatter.string(from: self)
    }
    
    public static func dateWithString(dateString: String, format: String, _ timeZone: TimeZone = .current, _ locale: Locale = .current) -> Date?{
        return getFormat(format, timeZone, locale).date(from: dateString)
    }
    
    public static func dateWithISOFormat(_ dateString: String) -> Date?{
        var formatter: DateFormatter!
        DispatchQueue.once(token: "jcKit.stringWithISOFormat") {
            formatter = DateFormatter.init()
            formatter.locale = Locale.init(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        return formatter.date(from: dateString)
    }
    
    private static func getFormat(_ format: String, _ timeZone: TimeZone = .current, _ locale: Locale = .current) -> DateFormatter {
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        formatter.locale = locale
        formatter.timeZone = timeZone
        return formatter
    }
    
}
