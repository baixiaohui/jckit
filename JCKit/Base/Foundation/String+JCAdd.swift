//
//  String+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/9.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension String {
    public static func stringWithUUID() -> String {
        let uuid = CFUUIDCreate(nil)
        let str = CFUUIDCreateString(nil, uuid)
        if str == nil {
            return ""
        }
        return str! as String
    }
    
    public var isLetter: Bool {
        let utf8Count = self.lengthOfBytes(using: String.Encoding.utf8)
        for loopIndex in 0..<utf8Count {
            let char = (self as NSString).character(at: loopIndex)
            if char < 65 || (char > 90 && char < 97) || char > 122 {
                return false
            }
        }
        return true
    }
    
    public var isAlllowercase: Bool {
        let utf8Count = self.lengthOfBytes(using: String.Encoding.utf8)
        for loopIndex in 0..<utf8Count {
            let char = (self as NSString).character(at: loopIndex)
            if char < 97 || char > 122 {
                return false
            }
        }
        return true
    }
    
    public var isAllCapital: Bool {
        let utf8Count = self.lengthOfBytes(using: String.Encoding.utf8)
        for loopIndex in 0..<utf8Count {
            let char = (self as NSString).character(at: loopIndex)
            if char < 65 || char > 90 {
                return false
            }
        }
        return true
    }
    
    public var isNumer: Bool {
        let utf8Count = self.lengthOfBytes(using: String.Encoding.utf8)
        for loopIndex in 0..<utf8Count {
            let char = (self as NSString).character(at: loopIndex)
            if char < 48 || char > 57 {
                return false
            }
        }
        return true
    }
    
    public var isNumerOrLetter: Bool {
        return self.isNumer || self.isLetter
    }
    
    public var isNumerAndLetter: Bool {
        let utf8Count = self.lengthOfBytes(using: String.Encoding.utf8)
        for loopIndex in 0..<utf8Count {
            let char = (self as NSString).character(at: loopIndex)
            if (char < 48 || char > 57) && (char < 65 || (char > 90 && char < 97) || char > 122) {
                return false
            }
        }
        return true
    }
    
    public func subStringWithIndex(_ num: Int) -> String{
        return self.subStringWithRange(0, num)
    }
    
    public func subStringWithRange(_ start: Int, _ end: Int) -> String {
        if start > end || start > self.count || end <= 0 || self.count == 0 {
            return self
        }
        var newEnd = end
        var newStart = start
        if start < 0 {
            newStart = 0
        }
        if end > self.count {
            newEnd = self.count
        }
        let index1 = self.index(self.startIndex, offsetBy: newStart)
        let index2 = self.index(self.startIndex, offsetBy: newEnd)
        return String(self[index1..<index2])
    }
}
