//
//  DispatchQueue+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/9.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension DispatchQueue {
    private static var onceTracker = [String]()
    public class func once(token: String, block: () -> ()) {
        objc_sync_enter(self)
        defer {
            objc_sync_exit(self)
        }
        if onceTracker.contains(token) {
            return
        }
        onceTracker.append(token)
        block()
    }
}
