//
//  NSObject+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/7.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation
extension NSObject {
    public func performSelector(_ sel: Selector, afterDelay daley: TimeInterval) {
        self.perform(sel, with: nil, afterDelay: daley)
    }
    
    public class func swizzleInstanceMethod(_ originalSel: Selector, _ newSel: Selector) -> Bool{
        let originalMethod = class_getInstanceMethod(self, originalSel)
        let newMethod = class_getInstanceMethod(self, newSel)
        if (originalMethod == nil) || newMethod == nil {
            return false
        }
        class_addMethod(self,
                        originalSel,
                        class_getMethodImplementation(self, originalSel)!,
                        method_getTypeEncoding(originalMethod!))
        class_addMethod(self,
                        newSel,
                        class_getMethodImplementation(self, newSel)!,
                        method_getTypeEncoding(newMethod!))
        
        method_exchangeImplementations(class_getInstanceMethod(self, originalSel)!,
                                       class_getInstanceMethod(self, newSel)!)
        return true
    }
    
    public class func swizzleClassMethod(_ originalSelector: Selector, _ swizzledSelector: Selector) -> Bool {
        if let cla = object_getClass(self) {
            let originalMethod = class_getInstanceMethod(cla, originalSelector)
            let swizzledMethod = class_getInstanceMethod(cla, swizzledSelector)
            if (originalMethod == nil) || swizzledMethod == nil {
                return false
            }
            method_exchangeImplementations(originalMethod!, swizzledMethod!)
            return true
        }
        return false
    }
    
    public func setAssociateValue(_ value: Any, withKey key: String) {
        objc_setAssociatedObject(self, key, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    public func setAssociateWeakValue(_ value: Any, withKey key: String) {
        objc_setAssociatedObject(self, key, value, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
    }
    
    public func removeAssociatedValues() {
        objc_removeAssociatedObjects(self)
    }
    
    public func getAssociatedValueForKey(_ key: String) -> Any{
        return objc_getAssociatedObject(self, key) as Any
    }
    
    public class func className() -> String{
        return NSStringFromClass(self)
    }
    
    public func className() -> String{
        return String.init(utf8String: class_getName(self.classForCoder)) ?? ""
    }
}
