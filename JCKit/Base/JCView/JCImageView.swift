//
//  JCImageView.swift
//  JCKit
//
//  Created by C J on 2020/10/23.
//  Copyright © 2020 JC. All rights reserved.
//

import Foundation

public class JCImageView: UIImageView {
    public var canLongPressOfSave: Bool = true {
        didSet {
            self.isUserInteractionEnabled = canLongPressOfSave
        }
    }
    public var saveImgBlock: JCBoolAction?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addGestureRecognizer(UILongPressGestureRecognizer.init(target: self, action: #selector(saveImg)))
    }
    
    @objc private func saveImg() {
        if !canLongPressOfSave {
            return
        }
        if self.image == nil {
            saveImgBlock?(false)
            return
        }
        let alert = UIAlertController.alert(title: "是否保存二维码", message: "", preferredStyle: .alert, cancelActionTitle: "点错了", cancelHandler: nil, defaultActionTitle: "是的呢") { [self] (_) in
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            guard let ctx = UIGraphicsGetCurrentContext() else { return  }
            layer.render(in: ctx)
            guard let img = UIGraphicsGetImageFromCurrentImageContext() else { return }
            UIGraphicsEndImageContext()
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(imaged), nil)
        }
        self.viewController?.present(alert, animated: true, completion: nil)
    }
    
    @objc private func imaged(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        saveImgBlock?(error == nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
