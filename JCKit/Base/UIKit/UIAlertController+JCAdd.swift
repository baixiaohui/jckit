//
//  UIAlertController.swift
//  JCKit
//
//  Created by C J on 2019/11/21.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UIAlertController {
    public class func alert(title: String?, message: String?, preferredStyle: UIAlertController.Style, cancelActionTitle: String?, cancelHandler: ((UIAlertAction) -> Void)?, defaultActionTitle: String?, defaultHandler: @escaping ((UIAlertAction) -> Void)) -> UIAlertController {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: preferredStyle)
        let cancelAction = UIAlertAction.init(title: cancelActionTitle, style: .cancel, handler: cancelHandler)
        let defaultAction = UIAlertAction.init(title: defaultActionTitle, style: .default, handler: defaultHandler)
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        return alert
    }
    
    public class func alert(title: String?, message: String?, defaultActionTitle: String?, defaultHandler: @escaping ((UIAlertAction) -> Void)) -> UIAlertController {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction.init(title: defaultActionTitle, style: .default, handler: defaultHandler)
        alert.addAction(defaultAction)
        return alert
    }
}
