//
//  UIView+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/12.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UIView {
    public func snapshotImage() -> UIImage? {
        if (UIGraphicsGetCurrentContext() == nil) {
            return nil
        }
        UIGraphicsBeginImageContextWithOptions(self.bSize, self.isOpaque, 0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let snap = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snap
    }
    
    public func snapshotImageAfterScreenUpdates(_ afterUpdates: Bool = false) -> UIImage? {
        if !self.responds(to: #selector(drawHierarchy(in:afterScreenUpdates:))) {
            return self.snapshotImage()
        }
        UIGraphicsBeginImageContextWithOptions(self.bSize, self.isOpaque, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: afterUpdates)
        let snap = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snap
    }
    
    public func snapshotPDF() -> NSData? {
        var bounds = self.bounds
        let data = NSMutableData.init()
        let consumer = CGDataConsumer.init(data: data as CFMutableData)
        if consumer == nil {
            return nil
        }
        let context = CGContext.init(consumer: consumer!, mediaBox: &bounds, nil)
        if context == nil {
            return nil
        }
        context?.beginPDFPage(nil)
        context?.translateBy(x: 0, y: bounds.size.height)
        context?.scaleBy(x: 1, y: -1)
        self.layer.render(in: context!)
        context?.endPDFPage()
        context?.closePDF()
        return data
    }
    
    public func setShadow(_ color: UIColor = .gray, radius: CGFloat = CGFloat(10), offSet: CGSize = CGSize.zero, opacity: Float = Float(1)) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = offSet
        self.layer.shadowOpacity = opacity
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    public func setBorder(_ bw: CGFloat = 1, _ bcolor: CGColor = UIColor.gray.cgColor) {
        self.layer.borderWidth = bw
        self.layer.borderColor = bcolor
    }
    
    public func removeAllSubViews() {
        while self.subviews.count > 0 {
            self.subviews.last?.removeFromSuperview()
        }
    }
    
    public func visibleAlpha() -> CGFloat {
        if self.isKind(of: UIWindow.classForCoder()) {
            return self.isHidden ? 0 : self.alpha
        }
        if self.window == nil {
            return 0
        }
        var v: UIView? = self
        var alpha = CGFloat(1.0)
        while v != nil {
            if v!.isHidden {
                alpha = 0
                break
            }
            alpha *= v!.alpha
            v = v?.superview
        }
        return alpha
    }
    
    public var viewController: UIViewController? {
        var nextResponder: UIResponder? = self
        repeat {
            nextResponder = nextResponder?.next
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
        } while nextResponder != nil
        return nil
    }
    
    public var left: CGFloat {
        
        get{
            return self.frame.origin.x
        }
        set{
            self.frame.origin.x = newValue
        }
    }
    
    public var top: CGFloat {
        
        get{
            return self.frame.origin.y
        }
        set{
            self.frame.origin.y = newValue
        }
    }
    
    public var right: CGFloat {
        
        get{
            return self.frame.size.width + self.left
        }
        set{
            self.frame.origin.x = newValue - self.frame.size.width
        }
    }
    
    public var bottom: CGFloat {
        
        get{
            return self.top + self.frame.size.height
        }
        set{
            self.frame.origin.y = newValue + self.frame.size.height
        }
    }
    
    public var centerX: CGFloat {
        
        get{
            return self.center.x
        }
        set{
            self.center = CGPoint.init(x: newValue, y: self.center.y)
        }
    }
    
    public var centerY: CGFloat {
        
        get{
            return self.center.y
        }
        set{
            self.center = CGPoint.init(x: self.center.x, y: newValue)
        }
    }
    
    public var width: CGFloat {
        
        get{
            return self.frame.size.width
        }
        set{
            self.frame.size.width = newValue
        }
    }
    
    public var height: CGFloat {
        
        get{
            return self.frame.size.height
        }
        set{
            self.frame.size.height = newValue
        }
    }
    
    public var size: CGSize {
        
        get{
            return self.frame.size
        }
        set{
            self.frame.size = newValue
        }
    }
    
    //bounds
    public var bLeft: CGFloat {
        
        get{
            return self.bounds.origin.x
        }
        set{
            self.bounds.origin.x = newValue
        }
    }
    
    public var bTop: CGFloat {
        
        get{
            return self.bounds.origin.y
        }
        set{
            self.bounds.origin.y = newValue
        }
    }
    
    public var bRight: CGFloat {
        
        get{
            return self.bounds.size.width + self.bLeft
        }
        set{
            self.bounds.origin.x = newValue - self.bounds.size.width
        }
    }
    
    public var bBottom: CGFloat {
        
        get{
            return self.bTop + self.bounds.size.height
        }
        set{
            self.bounds.origin.y = newValue + self.bounds.size.height
        }
    }
    
    public var bWidth: CGFloat {
        
        get{
            return self.bounds.size.width
        }
        set{
            self.bounds.size.width = newValue
        }
    }
    
    public var bHeight: CGFloat {
        
        get{
            return self.bounds.size.height
        }
        set{
            self.bounds.size.height = newValue
        }
    }
    
    public var bSize: CGSize {
        
        get{
            return self.bounds.size
        }
        set{
            self.bounds.size = newValue
        }
    }
}

extension UIView {
    public func clipRectCorner(direction: UIRectCorner = [.bottomLeft,.bottomLeft,.topLeft,.topRight], cornerRadius: CGFloat) {
        let cornerSize = CGSize(width: cornerRadius, height: cornerRadius)
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: direction, cornerRadii: cornerSize)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.addSublayer(maskLayer)
        layer.mask = maskLayer
    }
}

public struct UIRectSide : OptionSet {
    public let rawValue: Int
    public static let left = UIRectSide(rawValue: 1 << 0)
    public static let top = UIRectSide(rawValue: 1 << 1)
    public static let right = UIRectSide(rawValue: 1 << 2)
    public static let bottom = UIRectSide(rawValue: 1 << 3)
    public static let all: UIRectSide = [.top, .right, .left, .bottom]
    public init(rawValue: Int) {
        self.rawValue = rawValue;
    }
}

extension UIView{
    ///画虚线边框
    public func drawDashLine(strokeColor: UIColor, lineWidth: CGFloat = 1, lineLength: Int = 10, lineSpacing: Int = 5, corners: UIRectSide) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.bounds = self.bounds
        shapeLayer.anchorPoint = CGPoint(x: 0, y: 0)
        shapeLayer.fillColor = UIColor.blue.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        //每一段虚线长度 和 每两段虚线之间的间隔
        shapeLayer.lineDashPattern = [NSNumber(value: lineLength), NSNumber(value: lineSpacing)]
        let path = CGMutablePath()
        if corners.contains(.left) {
            path.move(to: CGPoint(x: 0, y: self.layer.bounds.height))
            path.addLine(to: CGPoint(x: 0, y: 0))
        }
        if corners.contains(.top){
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: self.layer.bounds.width, y: 0))
        }
        if corners.contains(.right){
            path.move(to: CGPoint(x: self.layer.bounds.width, y: 0))
            path.addLine(to: CGPoint(x: self.layer.bounds.width, y: self.layer.bounds.height))
        }
        if corners.contains(.bottom){
            path.move(to: CGPoint(x: self.layer.bounds.width, y: self.layer.bounds.height))
            path.addLine(to: CGPoint(x: 0, y: self.layer.bounds.height))
        }
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    ///画实线边框
    public func drawLine(strokeColor: UIColor, lineWidth: CGFloat = 1, corners: UIRectSide) {
        if corners == UIRectSide.all {
            self.layer.borderWidth = lineWidth
            self.layer.borderColor = strokeColor.cgColor
        }else{
            let shapeLayer = CAShapeLayer()
            shapeLayer.bounds = self.bounds
            shapeLayer.anchorPoint = CGPoint(x: 0, y: 0)
            shapeLayer.fillColor = UIColor.blue.cgColor
            shapeLayer.strokeColor = strokeColor.cgColor
            shapeLayer.lineWidth = lineWidth
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            let path = CGMutablePath()
            if corners.contains(.left) {
                path.move(to: CGPoint(x: 0, y: self.layer.bounds.height))
                path.addLine(to: CGPoint(x: 0, y: 0))
            }
            if corners.contains(.top){
                path.move(to: CGPoint(x: 0, y: 0))
                path.addLine(to: CGPoint(x: self.layer.bounds.width, y: 0))
            }
            if corners.contains(.right){
                path.move(to: CGPoint(x: self.layer.bounds.width, y: 0))
                path.addLine(to: CGPoint(x: self.layer.bounds.width, y: self.layer.bounds.height))
            }
            if corners.contains(.bottom){
                path.move(to: CGPoint(x: self.layer.bounds.width, y: self.layer.bounds.height))
                path.addLine(to: CGPoint(x: 0, y: self.layer.bounds.height))
            }
            shapeLayer.path = path
            self.layer.addSublayer(shapeLayer)
        }
    }
}
