//
//  UIImage+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/17.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation
import Accelerate

func _jc_CGImageSourceGetGIFFrameDelayAtIndex(_ source: CGImageSource, _ index: size_t) ->TimeInterval {
    var delay = TimeInterval(0)
    if let dic = CGImageSourceCopyPropertiesAtIndex(source, index, nil) {
        let dicGIF = CFDictionaryGetValue(dic, Unmanaged.passRetained(kCGImagePropertyGIFDictionary).autorelease().toOpaque())
        if dicGIF != nil {
            var num = Unmanaged<NSNumber>.fromOpaque(CFDictionaryGetValue((dicGIF as! CFDictionary), Unmanaged.passRetained(kCGImagePropertyGIFUnclampedDelayTime).autorelease().toOpaque())).takeUnretainedValue()
            if num.floatValue <= Float.ulpOfOne {
                num = Unmanaged<NSNumber>.fromOpaque(CFDictionaryGetValue((dicGIF as! CFDictionary), Unmanaged.passRetained(kCGImagePropertyGIFDelayTime).autorelease().toOpaque())).takeUnretainedValue()
            }
            delay = num.doubleValue
        }
    }
    return delay < 0.02 ? 0.1 : delay
}

extension UIImage {
    public static func imageWithSmallGIFData(_ data: Data, _ scale: CGFloat) -> UIImage?{
        let source = CGImageSourceCreateWithData(data as CFData, nil)
        if source == nil {
            return nil
        }
        let count = CGImageSourceGetCount(source!)
        if count <= 1 {
            return UIImage.init(data: data, scale: scale)
        }
        var frames = Array.init(repeating: 0, count: count)
        let oneFrameTime = 1 / 50.0
        var totalTime = TimeInterval(0)
        var totalFrame = 0
        var gcdFrame = 0
        (0..<count).forEach { (i) in
            let delay = _jc_CGImageSourceGetGIFFrameDelayAtIndex(source!, i)
            totalTime += delay
            var frame = lrint(delay / oneFrameTime)
            if frame < 1 {
                frame = 1
            }
            frames[i] = frame
            totalFrame += frames[i]
            if i == 0 {
                gcdFrame = frames[i]
            }else {
                var frame = frames[i]
                var tmp = 0
                if frame < gcdFrame {
                    (frame, gcdFrame) = (gcdFrame, frame)
                }
                while true {
                    tmp = frame % gcdFrame
                    if tmp == 0 {
                        frame = gcdFrame
                        gcdFrame = tmp
                    }
                }
            }
        }
        
        var array = [UIImage]()
        for i in 0..<count {
            let imageRef = CGImageSourceCreateImageAtIndex(source!, i, nil)
            if imageRef == nil {
                return nil
            }
            if imageRef?.width == 0 || imageRef?.height == 0 {
                return nil
            }
            let alphaInfo = (imageRef?.alphaInfo)!.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
            var hasAlpha = false
            if alphaInfo == CGImageAlphaInfo.premultipliedLast.rawValue || alphaInfo == CGImageAlphaInfo.premultipliedFirst.rawValue || alphaInfo == CGImageAlphaInfo.last.rawValue || alphaInfo == CGImageAlphaInfo.first.rawValue {
                hasAlpha = true
            }
            var bitmapInfo = CGBitmapInfo.byteOrder32Little.rawValue
            bitmapInfo = hasAlpha ? CGImageAlphaInfo.premultipliedFirst.rawValue : CGImageAlphaInfo.noneSkipFirst.rawValue
            let space = CGColorSpaceCreateDeviceRGB()
            let context = CGContext.init(data: nil, width: imageRef!.width, height: imageRef!.height, bitsPerComponent: 8, bytesPerRow: 0, space: space, bitmapInfo: bitmapInfo)
            if context == nil {
                return nil
            }
            context?.draw(imageRef!, in: CGRect.init(x: 0, y: 0, width: CGFloat(imageRef!.width), height:  CGFloat(imageRef!.height)))
            let decoded = context?.makeImage()
            if decoded == nil {
                return nil
            }
            let image = UIImage.init(cgImage: decoded!, scale: scale, orientation: UIImage.Orientation.up)
            (0..<frames[i] / gcdFrame).forEach { (i) in
                array.append(image)
            }
        }
        let image = UIImage.animatedImage(with: array, duration: totalTime)
        return image
    }
    
    public static func isAnimatedGIFData( _ data: Data) -> Bool {
        if data.count < 16 {
            return false
        }
        let magic = data.count & 0xFFFFFF
        if String(magic) != "\0FIG" {
            return false
        }
        let source = CGImageSourceCreateWithData(data as CFData, nil)
        if source == nil {
            return false
        }
        let count = CGImageSourceGetCount(source!)
        return count > 1
    }
    
    public static func isAnimatedGIFFile(_ path: String) -> Bool {
        if path.count == 0 {
            return false
        }
        let cpath = String(path.utf8)
        let fd = fopen(cpath, "rb")
        if fd == nil {
            return false
        }
        var isGIF = false
        var magic = 0
        if fread(&magic, MemoryLayout.size(ofValue: UInt32.self), 1, fd) == 1 && (String(magic & 0xFFFFFF) == "\0FIG"){
            isGIF = true
        }
        fclose(fd)
        return isGIF
    }
    
    public static func imageWithEmoji(_ emoji: String, _ size: CGFloat) -> UIImage?{
        if emoji.count == 0 || size < 1{
            return nil
        }
        let scale = UIScreen.main.scale
        let font = CTFontCreateWithName(__CFStringMakeConstantString("AppleColorEmoji"), size * scale, nil)
        let str = NSAttributedString.init(string: emoji, attributes: [kCTFontAttributeName as NSAttributedString.Key : font, kCTForegroundColorAttributeName as NSAttributedString.Key: UIColor.white.cgColor])
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let ctx = CGContext.init(data: nil, width: Int(size * scale), height: Int(size * scale), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageByteOrderInfo.orderDefault.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        ctx?.interpolationQuality = CGInterpolationQuality.high
        let line = CTLineCreateWithAttributedString(str)
        let bounds = CTLineGetBoundsWithOptions(line, CTLineBoundsOptions.useGlyphPathBounds)
        ctx?.textPosition = CGPoint.init(x: 0, y: -bounds.origin.y)
        let imageRef = ctx?.makeImage()
        if imageRef == nil {
            return nil
        }
        let image = UIImage.init(cgImage: imageRef!, scale: scale, orientation: UIImage.Orientation.up)
        return image
    }
    
    public static func imageWithPDF(_ dataOrPath: NSObject, _ resize: Bool = false, _ size: CGSize = .zero) -> UIImage?{
        var pdf: CGPDFDocument? = nil
        if dataOrPath.isEqual(Data.self) {
            let provider = CGDataProvider.init(data: dataOrPath as! CFData)
            if provider != nil {
                pdf = CGPDFDocument.init(provider!)
            }
        }else if dataOrPath.isEqual(String.self) {
            pdf = CGPDFDocument.init(URL.init(fileURLWithPath: dataOrPath as! String) as CFURL)
        }
        if pdf == nil {
            return nil
        }
        let page = pdf?.page(at: 1)
        if page == nil {
            return nil
        }
        let pdfRect = page!.getBoxRect(CGPDFBox.cropBox)
        let pdfSize = resize ? size : pdfRect.size
        let scale = UIScreen.main.scale
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let ctx = CGContext.init(data: nil, width: Int(pdfSize.width * scale), height: Int(pdfSize.height * scale), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageByteOrderInfo.orderDefault.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        if ctx == nil {
            return nil
        }
        ctx?.scaleBy(x: scale, y: scale)
        ctx?.translateBy(x: -pdfRect.origin.x, y: -pdfRect.origin.y)
        ctx?.drawPDFPage(page!)
        
        let image = ctx?.makeImage()
        if image == nil {
            return nil
        }
        let pdfImage = UIImage.init(cgImage: image!, scale: scale, orientation: UIImage.Orientation.up)
        return pdfImage
    }
    
    public static func imageWithColor(_ color: UIColor, _ size: CGSize = CGSize.init(width: 1, height: 1)) -> UIImage?{
        if color == UIColor.clear || size.width <= 0 || size.height <= 0 {
            return nil
        }
        let rect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    public static func imageWithSize(_ size: CGSize, _ drawBlock: ((CGContext)-> ())) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        if context == nil {
            return nil
        }
        drawBlock(context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    public func hasAlphaChannel() -> Bool {
        if self.cgImage == nil {
            return false
        }
        let alpha = self.cgImage!.alphaInfo.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        return alpha == CGImageAlphaInfo.first.rawValue || alpha == CGImageAlphaInfo.last.rawValue || alpha == CGImageAlphaInfo.premultipliedFirst.rawValue || alpha == CGImageAlphaInfo.premultipliedLast.rawValue
    }
    
   public  func drawInRect(cgRect: CGRect, withContentMode contentMode: UIView.ContentMode, clipsToBounds clips: Bool) {
        var rect = cgRect
        var size = self.size
        let drawRect = JCCGRectFitWithContentMode(&rect, &size, contentMode)
        if drawRect.size.width == 0 || drawRect.size.height == 0 {
            return
        }
        if clips {
            if let context = UIGraphicsGetCurrentContext() {
                context.saveGState()
                context.addRect(rect)
                context.clip()
                self.draw(in: drawRect)
            }
        }else {
            self.draw(in: drawRect)
        }
    }
    
   public func imageByResizeToSize( _ size: CGSize, _ contentMode: UIView.ContentMode? = nil) -> UIImage? {
        if size.width <= 0 || size.height <= 0 {
            return nil
        }
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        if contentMode != nil {
            self.drawInRect(cgRect: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: size), withContentMode: contentMode!, clipsToBounds: false)
        }else {
            self.draw(in: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: size))
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    public func imageByCropToRect(_ orRect: CGRect) -> UIImage?{
        var rect = orRect
        rect.origin.x = self.scale
        rect.origin.y = self.scale
        rect.size.width = self.scale
        rect.size.height = self.scale
        if rect.size.width <= 0 || rect.size.height <= 0 {
            return nil
        }
        let imageRef = self.cgImage?.cropping(to: rect)
        return UIImage.init(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
    }
    
    public func imageByInsetEdge(_ insets: UIEdgeInsets, _ color: UIColor? = nil) -> UIImage?{
        var size = self.size
        size.width -= insets.left + insets.right
        size.height -= insets.top + insets.bottom
        if size.width <= 0 || size.height <= 0 {
            return nil
        }
        let rect = CGRect.init(x: -insets.left, y: -insets.top, width: self.size.width, height: self.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()
        if color != nil {
            context?.setFillColor(color!.cgColor)
            let path = CGMutablePath()
            path.addRect(CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: size))
            path.addRect(rect)
            context?.addPath(path)
            context?.fillPath()
        }
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    public func imageByRoundCornerRadius(_ radius: CGFloat, _ oldCorners: UIRectCorner = UIRectCorner.allCorners, _ borderWidth: CGFloat = 0, _ borderColor: UIColor? = nil, _ borderLineJoin: CGLineJoin = CGLineJoin.miter) -> UIImage? {
        var corners = oldCorners
        if corners != UIRectCorner.allCorners {
            var tmp = UIRectCorner(rawValue: 0)
            if corners.rawValue & UIRectCorner.topLeft.rawValue != 0{
                tmp = UIRectCorner(rawValue: tmp.rawValue | UIRectCorner.bottomLeft.rawValue)
            }
            if corners.rawValue & UIRectCorner.topRight.rawValue != 0{
                tmp = UIRectCorner(rawValue: tmp.rawValue | UIRectCorner.bottomRight.rawValue)
            }
            if corners.rawValue & UIRectCorner.bottomLeft.rawValue != 0{
                tmp = UIRectCorner(rawValue: tmp.rawValue | UIRectCorner.topLeft.rawValue)
            }
            if corners.rawValue & UIRectCorner.bottomRight.rawValue != 0{
                tmp = UIRectCorner(rawValue: tmp.rawValue | UIRectCorner.topRight.rawValue)
            }
            corners = tmp
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()
        let rect = CGRect.init(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context?.scaleBy(x: 1, y: -1)
        context?.translateBy(x: 0, y: -rect.size.height)
        
        let minSize = min(self.size.width, self.size.height)
        if borderWidth < minSize / 2 {
            let path = UIBezierPath.init(roundedRect: rect.insetBy(dx: borderWidth, dy: borderWidth), byRoundingCorners: corners, cornerRadii: CGSize.init(width: radius, height: borderWidth))
            path.close()
            
            context?.saveGState()
            path.addClip()
            if self.cgImage != nil {
                context?.draw(self.cgImage!, in: rect)
            }
            context?.restoreGState()
        }
        
        if borderColor != nil && borderWidth < minSize / 2 && borderWidth > 0 {
            let strokeInset = (floor(borderWidth * self.scale) + 0.5) / self.scale
            let strokeRect = rect.insetBy(dx: strokeInset, dy: strokeInset)
            let strokeRadius = radius > self.scale / 2 ? radius - self.scale / 2 : 0
            let path = UIBezierPath.init(roundedRect: strokeRect, byRoundingCorners: corners, cornerRadii: CGSize.init(width: strokeRadius, height: borderWidth))
            path.close()
            
            path.lineWidth = borderWidth
            path.lineJoinStyle = borderLineJoin
            borderColor?.setStroke()
            path.stroke()
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    public func imageByRotate(_ radians: CGFloat, _ fitSize: Bool) -> UIImage?{
        let width = self.cgImage?.width ?? 0
        let height = self.cgImage?.height ?? 0
        let newRect = CGRect.init(x: 0, y: 0, width: width, height: height).applying(fitSize ? CGAffineTransform.init(rotationAngle: radians) : CGAffineTransform.identity)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext.init(data: nil, width: Int(newRect.size.width), height: Int(newRect.size.height), bitsPerComponent: 8, bytesPerRow: Int(newRect.size.width * 4), space: colorSpace, bitmapInfo: CGBitmapInfo.byteOrderMask.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        if context == nil {
            return nil
        }
        context?.setShouldAntialias(true)
        context?.setAllowsAntialiasing(true)
        context?.interpolationQuality = CGInterpolationQuality.high
        
        context?.translateBy(x: +(newRect.size.width * 0.5), y: +(newRect.size.height * 0.5))
        context?.rotate(by: radians)
        
        if self.cgImage == nil {
            return nil
        }
        context?.draw(self.cgImage!, in: CGRect.init(x: -(width / 2), y: -(height / 2), width: width, height: height))
        let imgRef = context?.makeImage()
        if imgRef == nil {
            return nil
        }
        let img = UIImage.init(cgImage: imgRef!, scale: self.scale, orientation: self.imageOrientation)
        return img
    }
    
    public func _jc_flipHorizontal(_ horizontal: Bool, _ vertical: Bool) -> UIImage?{
        if self.cgImage == nil {
            return nil
        }
        let width = self.cgImage?.width ?? 0
        let height = self.cgImage?.height ?? 0
        let bytesPerRow = width * 4
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext.init(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGBitmapInfo.byteOrderMask.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        if context == nil {
            return nil
        }
        
        context?.draw(self.cgImage!, in: CGRect.init(x: 0, y: 0, width: width, height: height))
        let data = context?.data
        if data == nil {
            return nil
        }
        
        var src = vImage_Buffer()
        src.width = vImagePixelCount(width)
        src.data = data
        src.rowBytes = bytesPerRow
        src.height = vImagePixelCount(height)
        
        var dest = src
        if vertical {
            vImageVerticalReflect_ARGB8888(&src, &dest, vImage_Flags(kvImageBackgroundColorFill))
        }
        if horizontal {
            vImageHorizontalReflect_ARGB8888(&src, &dest, vImage_Flags(kvImageBackgroundColorFill))
        }
        let imgRef = context?.makeImage()
        if imgRef == nil {
            return nil
        }
        let img = UIImage.init(cgImage: imgRef!, scale: self.scale, orientation: self.imageOrientation)
        
        return img
    }
    
    public func imageByRotateLeft90() -> UIImage?{
        return self.imageByRotate(90, true)
    }
    
    public func imageByRotateRight90() -> UIImage?{
        return self.imageByRotate(-90, true)
    }
    
    public func imageByRotate180() -> UIImage?{
        return self._jc_flipHorizontal(true, true)
    }
    
    public func imageByFlipVertical() -> UIImage?{
        return self._jc_flipHorizontal(false, true)
    }
    
    public func imageByFlipHorizontal() -> UIImage?{
        return self._jc_flipHorizontal(true, false)
    }

    public func imageByTintColor(_ color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let rect = CGRect.init(x: 0, y: 0, width: self.size.width, height: self.size.height)
        color.set()
        UIRectFill(rect)
        self.draw(at: CGPoint.init(x: 0, y: 0), blendMode: CGBlendMode.destinationIn, alpha: 1)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    public func imageByStyle(_ style: UIBlurEffect.Style) -> UIImage? {
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame.size = CGSize(width: self.size.width, height: self.size.height)
        UIGraphicsBeginImageContextWithOptions(blurView.bounds.size, false, self.scale)
        if UIGraphicsGetCurrentContext() == nil {
            return nil
        }
        blurView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
