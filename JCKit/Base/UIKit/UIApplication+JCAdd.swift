//
//  UIApplication+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/12.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UIApplication {
    public var documentsURL: URL? {
       return FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last
    }
    
    public var documentsPath: String? {
        return NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
    }
    
    public var cachesURL: URL? {
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last
    }
    
    public var cachesPath: String? {
        return NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
    }
    
    public var libraryURL: URL? {
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.libraryDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last
    }
    
    public var libraryPath: String? {
        return NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
    }
    
    public func isPirated() -> Bool {
        if UIDevice.isSimulator() || getgid() <= 10 || ((Bundle.main.infoDictionary?["SignerIdentity"]) != nil) || !_jc_fileExistInMainBundle("_CodeSignature") || !_jc_fileExistInMainBundle("SC_Info"){
            return true
        }
        return false
    }
    
    public func _jc_fileExistInMainBundle(_ name: String) -> Bool{
        let bundlePath = Bundle.main.bundlePath
        let path = bundlePath + "/" + name
        return FileManager.default.fileExists(atPath: path)
    }
    
    public var appBundleName: String {
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String {
            return name
        }
        return "Unknown"
    }
    
    public var appBundleID: String {
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String {
            return name
        }
        return "Unknown"
    }
    
    public var appVersion: String {
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            return name
        }
        return "Unknown"
    }
    
    public var appBuildVersion: String {
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String {
            return name
        }
        return "Unknown"
    }
}
