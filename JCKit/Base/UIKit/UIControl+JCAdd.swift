//
//  UIControl+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/6.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

class _JCUIControlBlockTarget: NSObject {
    var block: ((Any) -> Void)?
    var events: UIControl.Event?
    
    init(_ block: @escaping ((Any) -> Void), events: UIControl.Event) {
        super.init()
        self.block = block
        self.events = events
    }
    
    @objc func invoke(_ sender: Any) {
        if (block != nil) {
            block!(sender)
        }
    }
}

fileprivate var block_key = 0
extension UIControl {
    public func removeAllTargets() {
        let object = self.allTargets.enumerated()
        self.removeTarget(object, action: nil, for: UIControl.Event.allEvents)
        self._jc_allUIControlBlockTargets()?.removeAllObjects()
    }
    
    public func set(_ target: Any, _ action: Selector, _ controlEvents: UIControl.Event) {
        let targets = self.allTargets
        for currentTarget in targets {
            let actions = self.actions(forTarget: currentTarget, forControlEvent: controlEvents) ?? []
            for currentAction in actions {
                self.removeTarget(currentTarget, action: NSSelectorFromString(currentAction), for: controlEvents)
            }
        }
        self.addTarget(target, action: action, for: controlEvents)
    }
    
    public func setBlock(_ controlEvents: UIControl.Event, _ block: @escaping (Any)->Void) {
        self.removeAllBlocks(UIControl.Event.allEvents)
        self.addBlock(controlEvents, block)
    }
    
    public func removeAllBlocks(_ controlEvents: UIControl.Event) {
        let targets = self._jc_allUIControlBlockTargets() ?? []
        let removes = NSMutableArray.init()
        for tar in targets {
            if let target = tar as? _JCUIControlBlockTarget {
                if target.events != nil {
                    let newEvent = target.events!.rawValue & (~controlEvents.rawValue)
                    if newEvent > 0 {
                        self.removeTarget(target, action: #selector(target.invoke(_:)), for: target.events!)
                        target.events = UIControl.Event(rawValue: newEvent)
                        self.addTarget(target, action: #selector(target.invoke(_:)), for: target.events!)
                    }else {
                        self.removeTarget(target, action: #selector(target.invoke(_:)), for: target.events!)
                        removes.add(target)
                    }
                }
            }
        }
        targets.removeObjects(in: removes as! Array)
    }
    
    public func addBlock(_ controlEvents: UIControl.Event, _ block: @escaping (Any)->Void) {
        let target = _JCUIControlBlockTarget.init(block, events: controlEvents)
        self.addTarget(target, action: #selector(target.invoke(_:)), for: controlEvents)
        let targets = _jc_allUIControlBlockTargets()
        targets?.add(target)
    }
    
    func _jc_allUIControlBlockTargets() -> NSMutableArray? {
        var targets = objc_getAssociatedObject(self, &block_key)
        if targets != nil {
            targets = NSMutableArray.init()
            objc_setAssociatedObject(self, &block_key, targets, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        return targets as? NSMutableArray
    }
}
