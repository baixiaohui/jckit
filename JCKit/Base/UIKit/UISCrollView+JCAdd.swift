//
//  UISCrollView+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/6.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UIScrollView {
    public func scrollToTop(_ animated: Bool = true) {
        var off = self.contentOffset
        off.y = 0 - self.contentInset.top
        self.setContentOffset(off, animated: animated)
    }
    
    public func scrollToBottom(_ animated: Bool = true) {
        var off = self.contentOffset
        off.y = self.contentSize.height - self.bounds.size.height + self.contentInset.bottom
        self.setContentOffset(off, animated: animated)
    }
    
    public func scrollToLeft(_ animated: Bool = true) {
        var off = self.contentOffset
        off.y = 0 - self.contentInset.left
        self.setContentOffset(off, animated: animated)
    }
    
    public func scrollToRight(_ animated: Bool = true) {
        var off = self.contentOffset
        off.y = self.contentSize.width - self.bounds.size.width + self.contentInset.right
        self.setContentOffset(off, animated: animated)
    }
}
