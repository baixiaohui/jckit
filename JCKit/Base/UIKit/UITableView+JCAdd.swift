//
//  UITableView+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/11/21.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UITableView {
    public class func creat(_ rect: CGRect, _ tabCell: UITableViewCell) -> UITableView{
        let tab = UITableView.init(frame: rect, style: UITableView.Style.plain)
        tab.backgroundColor = UIColor.clear
        tab.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tab.separatorInset = UIEdgeInsets.init(top: 0, left: 56, bottom: 0, right: 30)
        tab.separatorColor = kRGBColorFromHex(rgbValue: 0xEDEDED)
        tab.showsVerticalScrollIndicator = false
        tab.register(tabCell.superclass, forCellReuseIdentifier: tab.className())
        return tab
    }
}
