//
//  UITextField+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/8/6.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UITextField {
    public class func creat(_ cgrect: CGRect) -> UITextField {
        let tf = UITextField.init(frame: cgrect)
        tf.layer.cornerRadius = 5
        tf.layer.masksToBounds = true
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 1
        return tf
    }
    
    public func selectAllText() {
        let range = self.textRange(from: self.beginningOfDocument, to: self.endOfDocument)
        self.selectedTextRange = range
    }
    
    public func setSelected(_ range: NSRange) {
        let beginning = self.beginningOfDocument
        let startPosition = self.position(from: beginning, offset: range.location)
        let endPosition = self.position(from: beginning, offset: NSMaxRange(range))
        if startPosition == nil || endPosition == nil {
            return
        }
        let selectionRange = self.textRange(from: startPosition!, to: endPosition!)
        self.selectedTextRange = selectionRange
    }
    
    public func placeHolderToCenter(_ font: CGFloat) {
        self.placeholderRect(forBounds: CGRect.init(x: 0, y: (self.font?.lineHeight)! - ((self.font?.lineHeight)! - UIFont.systemFont(ofSize: font).lineHeight) / 2.0, width: 0, height: 0))
    }
    
    public func setPlaceholderFont(_ font: CGFloat) {
        self.attributedPlaceholder = NSAttributedString.init(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize:font)])
    }
    
    public func setPlaceholderColor(_ color: UIColor) {
        self.attributedPlaceholder = NSAttributedString.init(string:self.placeholder ?? "", attributes: [
            NSAttributedString.Key.foregroundColor:UIColor.red])
    }
    
    ///过滤表情
    public func removeEmoji() -> String{
        if self.text?.count == 0 {
            return ""
        }
        let reges = try? NSRegularExpression.init(pattern: "[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]", options: NSRegularExpression.Options.caseInsensitive)
        let modifiedString = reges?.stringByReplacingMatches(in: self.text!, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, (self.text?.count)!), withTemplate: "")
        return modifiedString!
    }
    
    public func isIncludeChineseIn() -> Bool {
        if self.text?.count == 0 {
            return false
        }
        for (_, value) in self.text!.enumerated() {
            if ("\u{4E00}" <= value  && value <= "\u{9FA5}") {
                return true
            }
        }
        return false
    }
    
    /**  改变行间距  */
    public func changeLineSpace(_ space:CGFloat) {
        if self.text == nil || self.text == "" {
            return
        }
        let text = self.text
        let attributedString = NSMutableAttributedString.init(string: text!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = space
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: .init(location: 0, length: (text?.count)!))
        self.attributedText = attributedString
        self.sizeToFit()
    }
    
    /**  改变字间距  */
    public func changeWordSpace(_ space:CGFloat, cmp: (()->Void)? = nil) {
        if self.text == nil || self.text == "" {
            return
        }
        let text = self.text
        let attributedString = NSMutableAttributedString.init(string: text!, attributes: [NSAttributedString.Key.kern:space])
        let paragraphStyle = NSMutableParagraphStyle()
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: .init(location: 0, length: (text?.count)!))
        self.attributedText = attributedString
        self.sizeToFit()
        if cmp != nil {
            cmp!()
        }
    }
    
    /**  改变字间距和行间距  */
    public func changeSpace(lineSpace:CGFloat, wordSpace:CGFloat) {
        if self.text == nil || self.text == "" {
            return
        }
        let text = self.text
        let attributedString = NSMutableAttributedString.init(string: text!, attributes: [NSAttributedString.Key.kern:wordSpace])
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpace
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: .init(location: 0, length: (text!.count)))
        self.attributedText = attributedString
        self.sizeToFit()
    }
}
