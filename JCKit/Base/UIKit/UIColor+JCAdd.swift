//
//  UIColor+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/14.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

func CLAMP_COLOR_VALUE(_ v:inout CGFloat){
    v = v < 0 ? 0 : v > 1 ? 1 : v
}

public func JC_RGB2HSL(_ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat, _ h:inout CGFloat, _ s:inout CGFloat, _ l:inout CGFloat) {
    CLAMP_COLOR_VALUE(&r)
    CLAMP_COLOR_VALUE(&g)
    CLAMP_COLOR_VALUE(&b)
    var max, min, delta, sum: CGFloat
    max = fmax(r, fmax(g, b))
    min = fmin(r, fmin(g, b))
    delta = max - min
    sum = max + min
    if delta == 0 {
        h = 0
        s = 0
        return
    }
    s = delta / (sum < 1 ? sum : 2 - sum)
    if r == max {
        h = (g - b) / delta / 6
    }else if g == max {
        h = (2 + (b - r) / delta)
    }else {
        h = (4 + (r - g) / delta) / 6
    }
    if h < 0 {
        h += 1
    }
}

public func JC_HSL2RGB(_ h:inout CGFloat, _ s:inout CGFloat, _ l:inout CGFloat, _ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat) {
    CLAMP_COLOR_VALUE(&h)
    CLAMP_COLOR_VALUE(&s)
    CLAMP_COLOR_VALUE(&l)
    if s == 0 {
        r = 1
        g = 1
        b = 1
        return
    }
    var q = CGFloat(0)
    q = (l <= 0.5) ? (l * (1 + s)) : (l + s - (l * s))
    if q <= 0 {
        r = 0
        g = 0
        b = 0
    }else {
        r = 0
        g = 0
        b = 0
        var sextant = CGFloat(0)
        let m, sv, fract, vsf, mid1, mid2: CGFloat
        m = l + l - q
        sv = (q - m) / q
        if h == 1 {
            h = 0
        }
        h *= 6.0
        sextant = h
        fract = h - sextant
        vsf = q * sv * fract
        mid1 = m + vsf
        mid2 = q - vsf
        switch sextant {
        case 0: r = q; g = mid1; b = m; break
        case 1: r = mid2; g = q; b = m; break
        case 2: r = m; g = q; b = mid1; break
        case 3: r = m; g = mid2; b = q; break
        case 4: r = mid1; g = m; b = q; break
        case 5: r = q; g = m; b = mid2; break
        default:
            break
        }

    }
}

public func JC_RGB2HSB(_ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat, _ h:inout CGFloat, _ s:inout CGFloat, _ v:inout CGFloat) {
    CLAMP_COLOR_VALUE(&r)
    CLAMP_COLOR_VALUE(&g)
    CLAMP_COLOR_VALUE(&b)
    var max, min, delta: CGFloat
    max = fmax(r, fmax(g, b))
    min = fmin(r, fmin(g, b))
    delta = max - min
    
    v = max
    if delta == 0 {
        h = 0
        s = 0
        return
    }
    s = delta / max
    if r == max {
        h = (g - b) / delta / 6
    } else if g == max {
        h = (2 + (b - r) / delta) / 6
    } else {
        h = (4 + (r - g) / delta) / 6
    }
    if h < 0 {
        h += 1
    }
}

public func JC_HSB2RGB(_ h:inout CGFloat, _ s:inout CGFloat, _ v:inout CGFloat, _ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat) {
    CLAMP_COLOR_VALUE(&h)
    CLAMP_COLOR_VALUE(&s)
    CLAMP_COLOR_VALUE(&v)
    if s == 0 {
        r = v
        g = v
        b = v
    } else {
        var f, p, q, t, sextant: CGFloat
        if h == 1 {
            h = 0
        }
        h *= 6
        sextant = floor(h)
        f = h - sextant
        p = v * (1 - s)
        q = v * (1 - s * f)
        t = v * (1 - s * (1 - f))
        switch sextant {
        case 0: r = v; g = t; b = p; break
        case 1: r = q; g = v; b = p; break
        case 2: r = p; g = v; b = t; break
        case 3: r = p; g = q; b = v; break
        case 4: r = t; g = p; b = v; break
        case 5: r = v; g = p; b = q; break
        default:
            break
        }
    }
}

public func JC_RGB2CMYK(_ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat, _ c:inout CGFloat, _ m:inout CGFloat, _ y:inout CGFloat, _ k:inout CGFloat) {
    CLAMP_COLOR_VALUE(&r)
    CLAMP_COLOR_VALUE(&g)
    CLAMP_COLOR_VALUE(&b)
    c = 1 - r
    m = 1 - g
    y = 1 - b
    k = fmin(c, fmin(m, y))
    
    if k == 1 {
        c = 0
        m = 0
        y = 0
        k = 0
    } else {
        c = (c - k) / (1 - k)
        m = (m - k) / (1 - k)
        y = (y - k) / (1 - k)
    }
}

public func JC_CMYK2RGB(_ c:inout CGFloat, _ m:inout CGFloat, _ y:inout CGFloat, _ k:inout CGFloat, _ r:inout CGFloat, _ g:inout CGFloat, _ b:inout CGFloat) {
    CLAMP_COLOR_VALUE(&c)
    CLAMP_COLOR_VALUE(&m)
    CLAMP_COLOR_VALUE(&y)
    CLAMP_COLOR_VALUE(&k)
    r = (1 - c) * (1 - k)
    g = (1 - m) * (1 - k)
    b = (1 - y) * (1 - k)
}

public func JC_HSB2HSL(_ h:inout CGFloat, _ s:inout CGFloat, _ b:inout CGFloat, _ hh:inout CGFloat, _ ss:inout CGFloat, _ ll:inout CGFloat) {
    CLAMP_COLOR_VALUE(&h)
    CLAMP_COLOR_VALUE(&s)
    CLAMP_COLOR_VALUE(&b)
    hh = h
    ll = (2 - s) * b / 2
    if ll <= 0.5 {
        ss = (s) / ((2 - s))
    } else {
        ss = (s * b) / (2 - (2 - s) * b)
    }
}

public func JC_HSL2HSB(_ h:inout CGFloat, _ s:inout CGFloat, _ l:inout CGFloat, _ hh:inout CGFloat, _ ss:inout CGFloat, _ bb:inout CGFloat) {
    CLAMP_COLOR_VALUE(&h)
    CLAMP_COLOR_VALUE(&s)
    CLAMP_COLOR_VALUE(&l)
    hh = h
    if l <= 0.5 {
        bb = (s + 1) * l
        ss = (2 * s) / (s + 1)
    } else {
        bb = l + s * (1 - l)
        ss = (2 * s * (1 - l)) / bb
    }
}

extension UIColor {
    public static func colorWithHSL(hue: CGFloat, saturation:CGFloat, lightness: CGFloat, alpha:CGFloat) -> UIColor{
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var h = hue
        var s = saturation
        var l = lightness
        JC_HSL2RGB(&h, &s, &l, &r, &g, &b)
        return UIColor.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    public static func colorWithCMYK(cyan: CGFloat, magenta:CGFloat, yellow: CGFloat, black: CGFloat, alpha:CGFloat) -> UIColor{
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var c = cyan
        var m = magenta
        var y = yellow
        var bb = black
        JC_CMYK2RGB(&c, &m, &y, &bb, &r, &g, &b)
        return UIColor.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    public static func coloWithRGB(_ rgbValue: Int, _ alpha: CGFloat = 1) -> UIColor{
        return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                       green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                       blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                       alpha: alpha)
    }
    
    public static func coloWithRGBA(_ rgbaValue: UInt) -> UIColor{
        return UIColor(red: ((CGFloat)((rgbaValue & 0xFF000000) >> 24)) / 255.0,
                       green: ((CGFloat)((rgbaValue & 0xFF0000) >> 16)) / 255.0,
                       blue: ((CGFloat)((rgbaValue & 0xFF00) >> 8)) / 255.0,
                       alpha: ((CGFloat)(rgbaValue & 0xFF)) / 255.0)
    }
    
    public func rgbValue() -> Int {
        var r = CGFloat(0), g = CGFloat(0), b = CGFloat(0), a = CGFloat(0)
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let red = Int(r * 255)
        let green = Int(g * 255)
        let blue = Int(b * 255)
        return (red << 16) + (green << 8) + blue
    }
    
    public func rgbaValue() -> Int {
        var r = CGFloat(0), g = CGFloat(0), b = CGFloat(0), a = CGFloat(0)
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let red = Int(r * 255)
        let green = Int(g * 255)
        let blue = Int(b * 255)
        let alpha = Int(a * 255)
        return (red << 24) + (green << 16) + (blue << 8) + alpha
    }
    
    public static func hexStrToInt(_ str: String) -> Int {
        return Int.init(str, radix: 16) ?? 0
    }
    
    public var redValue: CGFloat {
        return colorValue().0
    }
    
    public var greenValue: CGFloat {
        return colorValue().1
    }
    
    public var blueValue: CGFloat {
        return colorValue().2
    }
    
    public var alpha: CGFloat {
        return self.cgColor.alpha
    }
    
    private func colorValue() -> (CGFloat, CGFloat, CGFloat, CGFloat) {
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return (r,g,b,a)
    }
    
    public var hueValue: CGFloat {
        return hueColorValue().0
    }
    
    public var saturationValue: CGFloat {
        return hueColorValue().1
    }
    
    public var brightnessValue: CGFloat {
        return hueColorValue().2
    }
    
    private func hueColorValue() -> (CGFloat, CGFloat, CGFloat, CGFloat) {
        var h = CGFloat(0)
        var s = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return (h,s,b,a)
    }
    
    public var colorSpaceModel: CGColorSpaceModel {
        return self.cgColor.colorSpace?.model ?? .unknown
    }
    
    public var jcCiColor: CIColor {
        return CIColor.init(color: self)
    }
}
