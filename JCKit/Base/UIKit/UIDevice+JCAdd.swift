//
//  UIDevice+JCAdd.swift
//  JCKit
//
//  Created by C J on 2019/5/9.
//  Copyright © 2019 JC. All rights reserved.
//

import Foundation

extension UIDevice {
    public static func systemVersion() -> Double {
        var version = Double(0)
        DispatchQueue.once(token: "jc.UIDevice.systemVersion") {
            version = Double(UIDevice.current.systemVersion) ?? 0
        }
    return version
    }
    
    public static func isPad() -> Bool {
        var bl = false
        DispatchQueue.once(token: "jc.UIDevice.isPad") {
            bl = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
        }
        return bl
    }
    
    public static func isSimulator() -> Bool {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }
    
    public static func isJailbroken() -> Bool{
        if self.isSimulator() {
            return false
        }
        let paths = ["/Applications/Cydia.app", "/private/var/lib/apt/", "/private/var/lib/cydia", "/private/var/stash"]
        for i in paths {
            if FileManager.default.fileExists(atPath: i) {
                return true
            }
        }
        let bash = fopen("/bin/bash", "r")
        if bash != nil {
            fclose(bash)
            return true
        }
        let path = "/private/"+String.stringWithUUID()
        let text = "test"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(path)
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                return true
            } catch {
                return false
            }
        }
        return false
    }
    
    public static func canMakePhoneCalls() -> Bool{
        var bl = false
        DispatchQueue.once(token: "jc.UIDevice.canMakePhoneCalls") {
            bl = UIApplication.shared.canOpenURL(URL.init(string: "tel://")!)
        }
        return bl
    }
    
    public struct jc_net_interface_counter {
        var en_in: UInt32 = 0
        var en_out: UInt32 = 0
        var pdp_ip_in: UInt32 = 0
        var pdp_ip_out: UInt32 = 0
        var awdl_in: UInt32 = 0
        var awdl_out: UInt32 = 0
    }
    public static func getIfi_ibytes() -> jc_net_interface_counter {
        var counter = jc_net_interface_counter()
        var addrsPointer: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&addrsPointer) == 0 {
            var pointer = addrsPointer
            while pointer != nil {
                if let addrs = pointer?.pointee {
                    let name = String(cString: addrs.ifa_name)
                    if addrs.ifa_addr.pointee.sa_family == UInt8(AF_LINK) {
                        let networkData = unsafeBitCast(addrs.ifa_data, to: UnsafeMutablePointer<if_data>.self)
                        if name.hasPrefix("en") { // Wifi
                            counter.en_in += networkData.pointee.ifi_ibytes
                            counter.en_out += networkData.pointee.ifi_obytes
                        } else if name.hasPrefix("pdp_ip") { // WWAN
                            counter.pdp_ip_in += networkData.pointee.ifi_ibytes
                            counter.pdp_ip_out += networkData.pointee.ifi_obytes
                        }else if name.hasPrefix("awdl") {
                            counter.awdl_in += networkData.pointee.ifi_ibytes
                            counter.awdl_out += networkData.pointee.ifi_obytes
                        }
                    }
                }
                pointer = pointer?.pointee.ifa_next
            }
            freeifaddrs(addrsPointer)
        }
        return counter
    }
    
    public static func ipAddressWithIfaName(_ name: String) -> String? {
        if name.count == 0 {
            return nil
        }
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    let ifaName: String = String(cString: (interface?.ifa_name)!)
                    if ifaName == name {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    public static func ipAddressWIFI() -> String{
        return self.ipAddressWithIfaName("en0") ?? ""
    }
    
    public static func ipAddressCell() -> String{
        return self.ipAddressWithIfaName("pdp_ip0") ?? ""
    }
    
    public static func machineModelName() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod1,1":  return "iPod Touch 1"
        case "iPod2,1":  return "iPod Touch 2"
        case "iPod3,1":  return "iPod Touch 3"
        case "iPod4,1":  return "iPod Touch 4"
        case "iPod5,1":  return "iPod Touch 5"
        case "iPod7,1":  return "iPod Touch 6"
            
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":  return "iPhone 4"
        case "iPhone4,1":  return "iPhone 4s"
        case "iPhone5,1":  return "iPhone 5"
        case "iPhone5,2":  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":  return "iPhone 5s"
        case "iPhone7,2":  return "iPhone 6"
        case "iPhone7,1":  return "iPhone 6 Plus"
        case "iPhone8,1":  return "iPhone 6s"
        case "iPhone8,2":  return "iPhone 6s Plus"
        case "iPhone8,4":  return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":  return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4": return "iPhone 8"
        case "iPhone10,2", "iPhone10,5": return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6": return "iPhone X"
        case "iPhone11,2": return "iPhone XS"
        case "iPhone11,4","iPhone11,6": return "iPhone XS Max"
        case "iPhone11,8": return "iPhone XR"
            
        case "iPad1,1": return "iPad"
        case "iPad1,2": return "iPad 3G"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":  return "iPad 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":  return "iPad Mini"
        case "iPad3,1", "iPad3,2", "iPad3,3":  return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":  return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":  return "iPad Air"
        case "iPad4,4", "iPad4,5", "iPad4,6":  return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":  return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":  return "iPad Mini 4"
        case "iPad5,3", "iPad5,4":  return "iPad Air 2"
        case "iPad6,3", "iPad6,4":  return "iPad Pro 9.7"
        case "iPad6,7", "iPad6,8":  return "iPad Pro 12.9"
            
        case "AppleTV2,1":  return "Apple TV 2"
        case "AppleTV3,1","AppleTV3,2":  return "Apple TV 3"
        case "AppleTV5,3":  return "Apple TV 4"
            
        case "i386", "x86_64":  return "Simulator"
            
        default:  return identifier
        }
    }
    
    public static func systemUpdatetime() -> Date {
        let time = ProcessInfo().systemUptime
        return Date.init(timeIntervalSinceNow: -time)
    }
    
    public static func diskFileSystem() -> NSDictionary?{
        do {
            let attrs = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())
            return attrs as NSDictionary
        } catch {
            return nil
        }
    }
    
    public static func diskSpace(_ fileKey: FileAttributeKey) -> Int64 {
        if let disk = diskFileSystem() {
            let space = disk.object(forKey: fileKey)
            if space as? Int64 == nil {
                return -1
            }
            let intSpace = space as! Int64
            if intSpace < 0 {
                return -1
            }
            return intSpace
        }
        return -1
    }
    
    public static func diskSpaceTotal() -> Int64 {
        return diskSpace(FileAttributeKey.systemSize)
    }
    
    public static func diskSpaceFree() -> Int64 {
        return diskSpace(FileAttributeKey.systemFreeSize)
    }
    
    public static func diskSpaceUsed() -> Int64 {
        let sys = diskSpaceTotal()
        let free = diskSpaceFree()
        if sys < 0 || free < 0 {
            return -1
        }
        let used = sys - free
        return used < 0 ? -1 : used
    }
    
    public static func memoryStatic() -> (data: vm_statistics_data_t, size: vm_size_t)?{
        let host_port = mach_host_self()
        var host_size = mach_msg_type_number_t(MemoryLayout.size(ofValue: vm_statistics_data_t()) / MemoryLayout.size(ofValue: integer_t()))
        var page_size = vm_size_t()
        host_page_size(host_port, &page_size)
        var vm_stat: vm_statistics = vm_statistics_data_t()
        let vmSta = vm_stat
        let status: kern_return_t = withUnsafeMutableBytes(of: &vm_stat) {
            let boundPtr = $0.baseAddress?.bindMemory(to: Int32.self, capacity: MemoryLayout.size(ofValue: vmSta) / MemoryLayout<Int32>.stride)
            return host_statistics(host_port, HOST_VM_INFO, boundPtr, &host_size)
        }
        if status != KERN_SUCCESS {
            return nil
        }
        return (vm_stat, page_size)
    }
    
    public static func memoryTotal() -> Int64 {
        let mem = ProcessInfo().physicalMemory
        return mem < 0 ? -1 : Int64(mem)
    }
    
    public static func memoryUsed() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.active_count + mem.data.inactive_count + mem.data.wire_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func memoryFree() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.free_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func memoryActive() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.active_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func memoryInactive() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.inactive_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func memoryWire() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.wire_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func memoryPurgeable() -> Int64 {
        if let mem = memoryStatic() {
            return Int64(mem.data.purgeable_count) * Int64(mem.size)
        }
        return  -1
    }
    
    public static func cpuCount() -> Int{
        return ProcessInfo().activeProcessorCount
    }
    
    public static func cpuUsage() -> CGFloat {
        var cpu = CGFloat(0)
        let cpus = cpuUsagePerProcessor()
        cpus.forEach { (num) in
            cpu += CGFloat.init(num)
        }
        return cpu
    }
    
    public static func cpuUsagePerProcessor()-> Array<Float> {
        var cpuUsageInfo: Array<Float> = []
        var cpuInfo: processor_info_array_t!
        var prevCpuInfo: processor_info_array_t?
        var numCpuInfo: mach_msg_type_number_t = 0
        var numPrevCpuInfo: mach_msg_type_number_t = 0
        var numCPUs: uint = 0
        let CPUUsageLock: NSLock = NSLock()
        
        let mibKeys: [Int32] = [ CTL_HW, HW_NCPU ]
        mibKeys.withUnsafeBufferPointer() { mib in
            var sizeOfNumCPUs: size_t = MemoryLayout<uint>.size
            let status = sysctl(processor_info_array_t(mutating: mib.baseAddress), 2, &numCPUs, &sizeOfNumCPUs, nil, 0)
            if status != 0 {
                numCPUs = 1
            }
        }
        
        var numCPUsU: natural_t = 0
        let err: kern_return_t = host_processor_info(mach_host_self(), PROCESSOR_CPU_LOAD_INFO, &numCPUsU, &cpuInfo, &numCpuInfo);
        if err == KERN_SUCCESS {
            CPUUsageLock.lock()
            for i in 0 ..< Int32(numCPUs) {
                var inUse: Int32
                var total: Int32
                if let prevCpuInfo = prevCpuInfo {
                    inUse = cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_USER)]
                        - prevCpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_USER)]
                        + cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_SYSTEM)]
                        - prevCpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_SYSTEM)]
                        + cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_NICE)]
                        - prevCpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_NICE)]
                    total = inUse + (cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_IDLE)]
                        - prevCpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_IDLE)])
                } else {
                    inUse = cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_USER)]
                        + cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_SYSTEM)]
                        + cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_NICE)]
                    total = inUse + cpuInfo[Int(CPU_STATE_MAX * i + CPU_STATE_IDLE)]
                }
                let coreInfo = Float(inUse) / Float(total)
                cpuUsageInfo.append(coreInfo)
                print(String(format: "Core: %u Usage: %f", i, Float(inUse) / Float(total)))
            }
            CPUUsageLock.unlock()
            
            if let prevCpuInfo = prevCpuInfo {
                let prevCpuInfoSize: size_t = MemoryLayout<integer_t>.stride * Int(numPrevCpuInfo)
                vm_deallocate(mach_task_self_, vm_address_t(UInt(bitPattern: prevCpuInfo)), vm_size_t(prevCpuInfoSize))
            }
            
            prevCpuInfo = cpuInfo
            numPrevCpuInfo = numCpuInfo
            
            cpuInfo = nil
            numCpuInfo = 0
        } else {
            print("Error!")
        }
        return cpuUsageInfo
    }
}
