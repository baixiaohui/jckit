//
//  JCKit.h
//  JCKit
//
//  Created by C J on 2019/5/7.
//  Copyright © 2019 JC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JCKit.
FOUNDATION_EXPORT double JCKitVersionNumber;

//! Project version string for JCKit.
FOUNDATION_EXPORT const unsigned char JCKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JCKit/PublicHeader.h>
